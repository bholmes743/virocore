package com.example.virosample

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.viro.core.*


class MiniMapView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var scnView: ViroViewScene? = null
    var scene: ARScene? = null

    init {
        setup()
    }

    private fun setup() {
        scnView = ViroViewScene(context, object : ViroViewScene.StartupListener {
            override fun onSuccess() {
                displayMiniMap()
            }

            override fun onFailure(p0: ViroViewScene.StartupError?, p1: String?) {
                TODO("Not yet implemented")
            }
        })
        addView(scnView)
    }


    private fun displayMiniMap() {
        scene = ARScene()

//        cameraNode = Node()
//        cameraNode?.name = "Camera Node"

        val box = Box(0.04f, 0.04f, 0.04f)
        val cubeNode = Node()
        cubeNode.geometry = box
        cubeNode.setPosition(Vector(0f, 0f, -1f))
        cubeNode.setScale(Vector(2f, 2f, 2f))
        scene!!.rootNode.addChildNode(cubeNode)



//        val texture = Texture(, Texture.Format.RGBA8, true, true)

//        self.cameraNode = scene.rootNode.childNode(withName: "camera", recursively: true)!
//        self.cameraNode.name = "Camera Node"
        // create and add a light to the scene
//        val liteNode = Node()

//        val lite = OmniLight()
//        lite.color = Color.WHITE.toLong()
//        lite.position = Vector(0f, 10f, 10f)
//        scene.rootNode.addLight(lite)

//        lightNode.lights = SCNLight()
//        lightNode.light!.type = .omni
//        lightNode.position = SCNVector3(x: 0, y: 10, z: 10)
//        scene.rootNode.addChildNode(lightNode)

        // create and add an ambient light to the scene
//        val ambientLight = AmbientLight()
//        ambientLight.color = Color.DKGRAY.toLong()
//        scene.rootNode.addLight(ambientLight)

//        let ambientLightNode = SCNNode()
//        ambientLightNode.light = SCNLight()
//        ambientLightNode.light!.type = .ambient
//        ambientLightNode.light!.color = UIColor.darkGray
//        scene.rootNode.addChildNode(ambientLightNode)

        // retrieve the ship node

        // animate the 3d object
        // retrieve the SCNView
        scnView?.scene = scene
//        self.scnView = SCNView(frame: self.bounds)
//        self.scnView.preferredFramesPerSecond = 15
//        // set the scene to the view
//        self.scnView.scene = scene
//
//        // allows the user to manipulate the camera
//        self.scnView.allowsCameraControl = true
//
//        // show statistics such as fps and timing information
//        self.scnView.showsStatistics = false
//
//        // configure the view

//        self.addSubview(self.scnView)
//
//        self.scnView.scene?.rootNode.addChildNode(self.geoNode)
//        self.scnView.backgroundColor = UIColor.black
//        let fovNode = SCNReferenceNode()
//        fovNode.position = SCNVector3(x: 0, y: 0, z: 0)
//
//
//        fovNode.name = "FOV Node"
//        let personNode = SCNNode(geometry: SCNSphere(radius: 0.4))
//        personNode.geometry?.firstMaterial?.diffuse.contents = UIColor.cyan
//        personNode.geometry?.firstMaterial?.lightingModel = .constant
//                personNode.worldPosition = SCNVector3(x: 0, y: 0, z: 0)
//        personNode.name = "Person Node"
//        fovNode.addChildNode(personNode)
//
//        let centerNode = SCNNode(geometry: SCNSphere(radius: 0.3))
//        centerNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
//        centerNode.geometry?.firstMaterial?.lightingModel = .constant
//                centerNode.worldPosition = SCNVector3(x: 0, y: 0, z: 0)
//        centerNode.name = "Center Node"
//        self.scnView.scene?.rootNode.addChildNode(centerNode)
//
//        self.scnView.scene?.rootNode.addChildNode(fovNode)
    }

    fun onStart(activity: Activity) {
        scnView!!.onActivityStarted(activity)
    }

    fun onResume(activity: Activity) {
        scnView!!.onActivityResumed(activity)
    }

    fun onPause(activity: Activity) {
        scnView!!.onActivityPaused(activity)
    }

   fun onStop(activity: Activity) {
       scnView!!.onActivityStopped(activity)
    }

    fun onDestroy(activity: Activity) {
        scnView!!.onActivityDestroyed(activity)
    }
}