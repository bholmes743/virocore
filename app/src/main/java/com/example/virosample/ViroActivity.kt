/*
 * Copyright (c) 2017-present, Viro, Inc.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.virosample

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.updateLayoutParams
import com.example.virosample.hello.R
import com.example.virosample.hello.databinding.MainActivityBinding
import com.viro.core.*
import com.viro.core.ARScene.TrackingStateReason
import com.viro.core.Vector
import java.io.IOException
import java.io.InputStream
import java.lang.ref.WeakReference
import java.util.*


/**
 * Activity that initializes Viro and ARCore. This activity builds an AR scene that continuously
 * detects planes. If you tap on a plane, an Android will appear at the location tapped. The
 * Androids are rendered using PBR (physically-based rendering).
 */
class ViroActivity : Activity() {
    private var mViroView: ViroViewScene? = null
    private var mScene: ARScene? = null
    private lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
//        mViroView = ViroViewARCore(this, object: ViroViewARCore.StartupListener {
//            override fun onSuccess() {
////                displayScene()
//                displayCube()
//            }
//
//            override fun onFailure(p0: ViroViewARCore.StartupError?, p1: String?) {
//                //no op
//            }
//
//        })
        mViroView = ViroViewScene(this, object : ViroViewScene.StartupListener {
            override fun onSuccess() {
                displayCube()
            }

            override fun onFailure(p0: ViroViewScene.StartupError?, p1: String?) {
                TODO("Not yet implemented")
            }
        })
        binding.constraintLayout.addView(mViroView)
        setContentView(binding.root)
    }

    fun showPopup(view: View?) {
        Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show()
    }

    fun displayCube() {
        mScene = ARScene()
        val box2 = Box(0.04f, 0.04f, 0.04f)
        val cubeNode2 = Node()
        cubeNode2.geometry = box2
        cubeNode2.setPosition(Vector(0f, 0f, -1f))
        cubeNode2.setScale(Vector(4f,4f,4f))
        cubeNode2.setRotation(Vector(0f, 0f, 2f))
        mScene!!.rootNode.addChildNode(cubeNode2)
        mViroView!!.scene = mScene
    }

    /**
     * Create an AR scene that tracks planes. Tapping on a plane places a 3D Object on the spot
     * tapped.
     */
    private fun displayScene() {
        // Create the 3D AR scene, and display the point cloud
        mScene = ARScene()
        mScene!!.displayPointCloud(true)

        // Create a TrackedPlanesController to visually display identified planes.
        val controller = TrackedPlanesController(this, mViroView)

        // Spawn a 3D Droid on the position where the user has clicked on a tracked plane.
        controller.addOnPlaneClickListener(object : ClickListener {
            override fun onClick(i: Int, node: Node, clickPosition: Vector) {
                createDroidAtPosition(clickPosition)
            }

            override fun onClickState(i: Int, node: Node, clickState: ClickState, vector: Vector) {
                //No-op
            }
        })


        mScene!!.setListener(controller)

        // Add some lights to the scene; this will give the Android's some nice illumination.
        val rootNode = mScene!!.rootNode
        val lightPositions: MutableList<Vector> = ArrayList()
        lightPositions.add(Vector(-10.0, 10.0, 1.0))
        lightPositions.add(Vector(10.0, 10.0, 1.0))
        val intensity = 300f
        val lightColors: MutableList<Int?> = arrayListOf()
        lightColors.add(Color.WHITE)
        lightColors.add(Color.WHITE)
        for (i in lightPositions.indices) {
            val light = OmniLight()
            light.setColor(lightColors[i]!!.toLong())
            light.position = lightPositions[i]
            light.attenuationStartDistance = 20f
            light.attenuationEndDistance = 30f
            light.intensity = intensity
            rootNode.addLight(light)
        }

        //Add an HDR environment map to give the Android's more interesting ambient lighting.
        val environment = Texture.loadRadianceHDRTexture(Uri.parse("file:///android_asset/ibl_newport_loft.hdr"))
        mScene!!.lightingEnvironment = environment
        mViroView!!.scene = mScene
    }

    /**
     * Create an Android object and have it appear at the given location.
     * @param position The location where the Android should appear.
     */
    private fun createDroidAtPosition(position: Vector) {
        // Create a droid on the surface
        val bot = getBitmapFromAsset(this, "andy.png")
        val object3D = Object3D()
        object3D.setPosition(position)
        mScene!!.rootNode.addChildNode(object3D)

        // Load the Android model asynchronously.
        object3D.loadModel(mViroView!!.viroContext, Uri.parse("file:///android_asset/andy.obj"), Object3D.Type.OBJ, object : AsyncObject3DListener {
            override fun onObject3DLoaded(`object`: Object3D, type: Object3D.Type) {
                // When the model is loaded, set the texture associated with this OBJ
                val objectTexture = Texture(bot, Texture.Format.RGBA8, false, false)
                val material = Material()
                material.diffuseTexture = objectTexture

                // Give the material a more "metallic" appearance, so it reflects the environment map.
                // By setting its lighting model to PHYSICALLY_BASED, we enable PBR rendering on the
                // model.
                material.roughness = 0.23f
                material.metalness = 0.7f
                material.lightingModel = Material.LightingModel.PHYSICALLY_BASED
                object3D.geometry.materials = Arrays.asList(material)
            }

            override fun onObject3DFailed(s: String) {}
        })

        // Make the object draggable.
        object3D.dragListener = DragListener { i, node, vector, vector1 ->
            // No-op.
        }
        object3D.dragType = Node.DragType.FIXED_DISTANCE
    }

    /**
     * Tracks planes and renders a surface on them so the user can see where we've identified
     * planes.
     */
    private class TrackedPlanesController(activity: Activity, rootView: View?) : ARScene.Listener {
        private val mCurrentActivityWeak: WeakReference<Activity>
        private var searchingForPlanesLayoutIsVisible = false
        private val surfaces = HashMap<String, Node>()
        private val mPlaneClickListeners: MutableSet<ClickListener> = HashSet()
        override fun onTrackingUpdated(trackingState: ARScene.TrackingState, trackingStateReason: TrackingStateReason) {
            //no-op
        }

        fun addOnPlaneClickListener(listener: ClickListener) {
            mPlaneClickListeners.add(listener)
        }

        fun removeOnPlaneClickListener(listener: ClickListener) {
            if (mPlaneClickListeners.contains(listener)) {
                mPlaneClickListeners.remove(listener)
            }
        }

        /**
         * Once a Tracked plane is found, we can hide the our "Searching for Surfaces" UI.
         */
        private fun hideIsTrackingLayoutUI() {
            if (searchingForPlanesLayoutIsVisible) {
                return
            }
            searchingForPlanesLayoutIsVisible = true
            val activity = mCurrentActivityWeak.get() ?: return
            val isTrackingFrameLayout = activity.findViewById<View>(R.id.viro_view_hud)
            isTrackingFrameLayout.animate().alpha(0.0f).duration = 2000
        }

        override fun onAnchorFound(arAnchor: ARAnchor, arNode: ARNode) {
            // Spawn a visual plane if a PlaneAnchor was found
            if (arAnchor.type == ARAnchor.Type.PLANE) {
                val planeAnchor = arAnchor as ARPlaneAnchor

                // Create the visual geometry representing this plane
                val dimensions = planeAnchor.extent
                val plane = Surface(1f, 1f)
                plane.width = dimensions.x
                plane.height = dimensions.z

                // Set a default material for this plane.
                val material = Material()
                material.diffuseColor = Color.parseColor("#BF000000")
                plane.materials = Arrays.asList(material)

                // Attach it to the node
                val planeNode = Node()
                planeNode.geometry = plane
                planeNode.setRotation(Vector(-Math.toRadians(90.0), 0.0, 0.0))
                planeNode.setPosition(planeAnchor.center)

                // Attach this planeNode to the anchor's arNode
                arNode.addChildNode(planeNode)
                surfaces[arAnchor.getAnchorId()] = planeNode

                // Attach click listeners to be notified upon a plane onClick.
                planeNode.clickListener = object : ClickListener {
                    override fun onClick(i: Int, node: Node, vector: Vector) {
                        for (listener in mPlaneClickListeners) {
                            listener.onClick(i, node, vector)
                        }
                    }

                    override fun onClickState(i: Int, node: Node, clickState: ClickState, vector: Vector) {
                        //No-op
                    }
                }

                // Finally, hide isTracking UI if we haven't done so already.
                hideIsTrackingLayoutUI()
            }
        }

        override fun onAnchorUpdated(arAnchor: ARAnchor, arNode: ARNode) {
            if (arAnchor.type == ARAnchor.Type.PLANE) {
                val planeAnchor = arAnchor as ARPlaneAnchor

                // Update the mesh surface geometry
                val node = surfaces[arAnchor.getAnchorId()]
                val plane = node!!.geometry as Surface
                val dimensions = planeAnchor.extent
                plane.width = dimensions.x
                plane.height = dimensions.z
            }
        }

        override fun onAnchorRemoved(arAnchor: ARAnchor, arNode: ARNode) {
            surfaces.remove(arAnchor.anchorId)
        }

        override fun onTrackingInitialized() {
            //No-op
        }

        override fun onAmbientLightUpdate(lightIntensity: Float, lightColor: Vector) {
            //No-op
        }

        init {
            mCurrentActivityWeak = WeakReference(activity)
            // Inflate viro_view_hud.xml layout to display a "Searching for surfaces" text view.
            View.inflate(activity, R.layout.viro_view_hud, rootView as ViewGroup?)
        }
    }

    private fun getBitmapFromAsset(context: Context, assetName: String): Bitmap? {
        val assetManager = context.resources.assets
        val imageStream: InputStream
        imageStream = try {
            assetManager.open(assetName)
        } catch (exception: IOException) {
            Log.w(TAG, "Unable to find image [" + assetName + "] in assets! Error: "
                    + exception.message)
            return null
        }
        return BitmapFactory.decodeStream(imageStream)
    }

    override fun onStart() {
        super.onStart()
        binding.miniMapView.onStart(this)
        mViroView!!.onActivityStarted(this)
    }

    override fun onResume() {
        super.onResume()
        binding.miniMapView.onResume(this)
        mViroView!!.onActivityResumed(this)
    }

    override fun onPause() {
        super.onPause()
        binding.miniMapView.onPause(this)
        mViroView!!.onActivityPaused(this)
    }

    override fun onStop() {
        super.onStop()
        binding.miniMapView.onStart(this)
        mViroView!!.onActivityStopped(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        mViroView!!.onActivityDestroyed(this)
    }

    companion object {
        private val TAG = ViroActivity::class.java.simpleName
    }
}